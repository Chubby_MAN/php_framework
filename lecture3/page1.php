<h1>lecture_3</h1>
<h1>page 1</h1>
<?php

    class person{
        public $name;
        public $last_name;
        private $age = 19;
        protected $id = "82349234982";
        

        protected function print_php_info(){
            var_dump(phpinfo());
        }


        private function print_class_name(){
            echo "<h3>person</h3>";
        }


        public function print_name(){
            echo $this->name;
        }  

        public function print_age(){
            $this->print_class_name();
            echo $this->age;
        }   

        public function get_age(){
            return $this->age;
        }   
        
        public function get_id(){
            return $this->id;
        }
    
    }

    $person1 = new person();
    $person1->name = "victoria";
    // $person1->age = "19";
    // $person1->id = "023874234";

    $person1->print_age();
    $age2= $person1->get_age()+10;
    echo"<br><h1>age={$age2}</h1>";
    // $person1->print_php_info();

?>